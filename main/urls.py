"""S3CRUD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from main import views 


app_name = "main"


urlpatterns = [
    path('', views.IndexAPIView.as_view(), name="index"),
    path('buckets/create/', views.BucketCreateAPIView.as_view(), name="buckets-create"),
    path('buckets/list/', views.BucketListAPIView.as_view(), name="buckets-list"),
    path('buckets/delete/', views.BucketDeleteAPIView.as_view(), name="buckets-delete"),
    path('buckets/exists/<slug:bucket_name>/', views.BucketExistsAPIView.as_view(), name="buckets-exists"),
    
    path('objects/list/<slug:bucket_name>/', views.ObjectListAPIView.as_view(), name="objects-list"),
    path('objects/list/<slug:bucket_name>/<slug:object_name>/', views.ObjectListAPIView.as_view(), name="objects-list"),
    
    path('folders/create/', views.FolderCreateAPIView.as_view(), name="folders-create"),
    path('folders/delete/', views.FolderDeleteAPIView.as_view(), name="folders-delete"),
    path('folders/exists/<slug:bucket_name>/<slug:folder_name>/', views.FolderExistsAPIView.as_view(), name="folders-exists"),
    
    path('files/upload/', views.FileUploadAPIView.as_view(), name="files-upload"),
    path('files/download/', views.FolderCreateAPIView.as_view(), name="files-download"),
    path('files/delete/', views.FolderCreateAPIView.as_view(), name="files-delete"),
    path('files/exists/', views.FolderCreateAPIView.as_view(), name="files-exists"),

]
