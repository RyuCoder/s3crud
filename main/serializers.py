from rest_framework import serializers


class FileUploadSerializer(serializers.Serializer):

    bucket_name = serializers.CharField()
    folder_name = serializers.CharField()
    file_item = serializers.FileField()
