from pprint import pprint 

from django.conf import settings

from rest_framework.response import Response
from rest_framework.views import APIView

import boto3

from botocore.exceptions import ClientError

from main.serializers import FileUploadSerializer


class IndexAPIView(APIView):

    def get(self, request):
        return Response("Welcome")


class BucketCreateAPIView(APIView):

    def post(self, request, *args, **kwargs):

        if request.method != "POST":
            message = "Only POST requests are allowed on this endpoint."
            return Response(message)

        if request.content_type == "application/json":
            bucket_name = request.data.get("bucket_name", None)
            region = request.data.get("region", None)
        else:
            bucket_name = request.POST.get("bucket_name", None)
            region = request.POST.get("region", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        if region == None:
            region = settings.AWS_DEFAULT_REGION    # "ap-south-1" for India

        try:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name, CreateBucketConfiguration=location)
            
            message = 'Created bucket {0} in the S3 region ({1})'.format(bucket_name, region)
            return Response(message)

        except ClientError as e: 
            return Response(e.response["Error"]["Message"])
        except Exception as e: 
            return Response(str(e))

        message = {"error" : "Something went wrong in BucketCreateAPIView."}
        return Response(message)


class BucketListAPIView(APIView):

    def get(self, request):

        if request.method != "GET":
            message = "Only GET requests are allowed on this endpoint."
            return Response(message)

        s3_client = boto3.resource('s3')

        buckets = [] 

        for bucket in s3_client.buckets.all():
            buckets.append(bucket.name)

        return Response(buckets)


class BucketDeleteAPIView(APIView):

    def post(self, request, *args, **kwargs):

        if request.method != "POST":
            message = "Only POST requests are allowed on this endpoint."
            return Response(message)

        if request.content_type == "application/json":
            bucket_name = request.data.get("bucket_name", None)
        else:
            bucket_name = request.POST.get("bucket_name", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        try:
            s3_client = boto3.client('s3')
            s3_client.delete_bucket(Bucket=bucket_name)
            
            message = 'Bucket {0} deleted successfully.'.format(bucket_name)
            return Response(message)

        except ClientError as e: 
            return Response(e.response["Error"]["Message"])
        except Exception as e: 
            return Response(str(e))

        message = {"error" : "Something went wrong in BucketDeleteAPIView."}
        return Response(message)


class BucketExistsAPIView(APIView):

    def get(self, request, *args, **kwargs):

        if request.method != "GET":
            message = "Only GET requests are allowed on this endpoint."
            return Response(message)

        # This will not work for the local python script
        bucket_name = kwargs.get("bucket_name", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        try:
            s3_client = boto3.client('s3')
            response = s3_client.head_bucket(Bucket=bucket_name)

            if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                message = '{0} exists and you have permission to access it.'.format(bucket_name)
            else:
                message = 'Man! Something Else Went Wrong.'
            
            return Response(message)

        except ClientError as e:
            return Response(e.response["Error"]["Message"])
        except Exception as e: 
            return Response(str(e))

        message = {"error" : "Something went wrong in BucketExistsAPIView."}
        return Response(message)


class ObjectListAPIView(APIView):

    def get(self, request, *args, **kwargs):
            
        if request.method != "GET":
            message = "Only GET requests are allowed on this endpoint."
            return Response(message)

        bucket_name = kwargs.get("bucket_name", None)
        object_name = kwargs.get("object_name", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        objects = []

        if object_name is None:
            object_name = ''

        try:
            s3_client = boto3.client('s3')
            results = s3_client.list_objects(Bucket=bucket_name, Prefix=object_name)

            for item in results['Contents']:
                objects.append(item['Key'])

            return Response(objects)

        except Exception as e:
            return Response(str(e))

        message = {"error" : "Something went wrong in ObjectListAPIView."}
        return Response(message)


class FolderCreateAPIView(APIView):

    def post(self, request, *args, **kwargs):

        if request.method != "POST":
            message = "Only POST requests are allowed on this endpoint."
            return Response(message)

        if request.content_type == "application/json":
            bucket_name = request.data.get("bucket_name", None)
            folder_name = request.data.get("folder_name", None)
            object_name = request.data.get("object_name", None)
        else:
            bucket_name = request.POST.get("bucket_name", None)
            folder_name = request.POST.get("folder_name", None)
            object_name = request.POST.get("object_name", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        if folder_name == None:
            message = "folder_name must be passed."
            return Response(message)
        
        if object_name:
            object_name = object_name + folder_name + '/'
        else:
            object_name = folder_name + '/'

        try:
            s3_client = boto3.client('s3')

            s3_client.put_object(Bucket=bucket_name, Key=object_name)

            message = 'Added {0} to {1}'.format(object_name, bucket_name)
            return Response(message)
        
        except ClientError as e:
            return Response(e.response["Error"]["Message"])
        except Exception as e:
            return Response(str(e))
        
        message = {"error" : "Something went wrong in FolderCreateAPIView."}
        return Response(message)


class FolderDeleteAPIView(APIView):

    def post(self, request, *args, **kwargs):

        if request.method != "POST":
            message = "Only POST requests are allowed on this endpoint."
            return Response(message)

        if request.content_type == "application/json":
            bucket_name = request.data.get("bucket_name", None)
            folder_name = request.data.get("folder_name", None)
        else:
            bucket_name = request.POST.get("bucket_name", None)
            folder_name = request.POST.get("folder_name", None)
        
        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        if folder_name == None:
            message = "folder_name must be passed."
            return Response(message)

        objects = []
        if folder_name[-1]!='/':
            folder_name += '/'

        try:
            s3_client = boto3.client('s3')
            results = s3_client.list_objects(Bucket=bucket_name, Prefix=folder_name)

            for item in results['Contents']:
                objects.append(item['Key'])
        
            for obj in objects:
                s3_client.delete_object(Bucket=bucket_name, Key=obj)

            message = '{0} was deleted from {1}'.format(folder_name, bucket_name)
            return Response(message)

        except ClientError as e:
            return Response(e.response["Error"]["Message"])
        except Exception as e:
            return Response(str(e))

        message = {"error" : "Something went wrong in FolderDeleteAPIView."}
        return Response(message)


class FolderExistsAPIView(APIView):

    def get(self, request, *args, **kwargs):

        if request.method != "GET":
            message = "Only GET requests are allowed on this endpoint."
            return Response(message)

        bucket_name = kwargs.get("bucket_name", None)
        folder_name = kwargs.get("folder_name", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        if folder_name == None:
            message = "folder_name must be passed."
            return Response(message)

        objects = [] 

        try:
            s3_client = boto3.client('s3')
            results = s3_client.list_objects(Bucket=bucket_name, Prefix=folder_name)
            
            if "Contents" in results.keys():
                for i in results['Contents']:
                    objects.append(i['Key'])

                if objects !=[]:
                    message = '{0} found in {1}'.format(folder_name, bucket_name)
                else:
                    message = '{0} not found in {1}'.format(folder_name, bucket_name)
            else:
                message = '{0} not found in {1}'.format(folder_name, bucket_name)

            return Response(message)

        except ClientError as e:
             return Response(e.response["Error"]["Message"])
        except Exception as e:
            return Response(str(e))

        message = {"error" : "Something went wrong in BucketExistsAPIView."}
        return Response(message)


class FileUploadAPIView(APIView):

    # serializer is not used during the processing of the view
    # it is only used to show the form during uploading
    # Ideally, view should be processes using the serializer_class 
    serializer_class = FileUploadSerializer

    def post(self, request, *args, **kwargs):

        if request.method != "POST":
            message = "Only POST requests are allowed on this endpoint."
            return Response(message)

        bucket_name = request.POST.get("bucket_name", None)
        folder_name = request.POST.get("folder_name", None)
        file_item = request.FILES.get("file_item", None)

        if bucket_name == None:
            message = "bucket_name must be passed."
            return Response(message)

        if folder_name == None:
            message = "folder_name must be passed."
            return Response(message)

        if file_item == None:
            message = "file_item must be passed."
            return Response(message)


        # If S3 object_name was not specified, use file_name
        object_name = file_item.name

        if folder_name != "":
            object_name = folder_name + "/" + object_name


        try:
            s3_client = boto3.client('s3')
            response = s3_client.upload_fileobj(file_item.file, bucket_name, object_name)

            message = 'File {0} uploaded successfully.'.format(object_name)
            return Response(message)

        except ClientError as e:
            return Response(e.response["Error"]["Message"])
        except Exception as e:
            return Response(str(e))

        message = {"error" : "Something went wrong in FileUploadAPIView."}
        return Response(message)

