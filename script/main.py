import requests
import os 

from pprint import pprint


# CONSTANTS 
DOMAIN_NAME = "http://127.0.0.1:8000/"

BUCKETS_CREATE_URL  = DOMAIN_NAME + "buckets/create/"
BUCKETS_LIST_URL    = DOMAIN_NAME + "buckets/list/"
BUCKETS_DELETE_URL  = DOMAIN_NAME + "buckets/delete/"
BUCKETS_EXISTS_URL  = DOMAIN_NAME + "buckets/exists/"

OBJECTS_LIST_URL    = DOMAIN_NAME + "objects/list/"

FOLDERS_CREATE_URL  = DOMAIN_NAME + "folders/create/"
FOLDERS_DELETE_URL  = DOMAIN_NAME + "folders/delete/"
FOLDERS_EXISTS_URL  = DOMAIN_NAME + "folders/exists/"

FILES_UPLOAD_URL    = DOMAIN_NAME + "files/upload/"
FILES_DOWNLOAD_URL  = DOMAIN_NAME + "files/download/"
FILES_DELETE_URL    = DOMAIN_NAME + "files/delete/"
FILES_EXISTS_URL    = DOMAIN_NAME + "files/exists/"


def buckets_list():
    response = requests.get(BUCKETS_LIST_URL)
    buckets = response.json()
    return buckets 


def buckets_create(bucket_name, region=None):
    data = {"bucket_name" : bucket_name, "region" : region}
    response = requests.post(BUCKETS_CREATE_URL, data=data)
    return response.json()


def buckets_delete(bucket_name):
    data = {"bucket_name" : bucket_name}
    response = requests.delete(BUCKETS_DELETE_URL, data=data)
    return response.text


def buckets_exists(bucket_name):
    url = BUCKETS_EXISTS_URL + bucket_name + "/"
    response = requests.get(url)
    return response.text


def objects_list(bucket_name, object_name=None):
    url = OBJECTS_LIST_URL + bucket_name + "/"

    if object_name != None:
        url += object_name + "/"

    response = requests.get(url)
    objects = response.json()
    return objects 


def folders_create(bucket_name, folder_name, object_name=None):
    data = {"bucket_name" : bucket_name, "folder_name":folder_name, "object_name": object_name}
    response = requests.post(FOLDERS_CREATE_URL, data=data)
    return response.json()


def folders_delete(bucket_name, folder_name):
    data = {"bucket_name" : bucket_name, "folder_name": folder_name}
    response = requests.post(FOLDERS_DELETE_URL, data=data)
    return response.text


def folders_exists(bucket_name, folder_name):
    url = FOLDERS_EXISTS_URL + bucket_name + "/" + folder_name + "/"
    response = requests.get(url)
    return response.text


def files_upload(bucket_name, folder_name, file_name, object_name=None): 
    directory = os.path.dirname(os.path.abspath(__file__))
    abs_file_name = os.path.join(directory, file_name)

    files = {'file': open(abs_file_name, 'rb')}

    data = {"bucket_name" : bucket_name, "folder_name": folder_name,
            "object_name": object_name}

    response = requests.post(FILES_UPLOAD_URL, data=data, files=files)
    return response.text 


def validate_arguments(arguments):

    # Code for validation 
    # Different operations will have different mandatory variables
    # Their checking should happen here

    return arguments


def parse_arguments():
    arguments = []
    
    # Code to read arguments goes here

    # Check if the mandatory arguments to operations are passed or not
    arguments = validate_arguments(arguments)

    return arguments 


def execute_operation(operation, *args, **kwargs):
    """
        Factory Method for executing operation
    """

    results = []

    # print()
    # print(args)
    # print()
    # print(kwargs)
    # print()

    if operation == "buckets-list":
        results = buckets_list()
    elif operation == "buckets-create":
        results = buckets_create(kwargs["bucket_name"], kwargs["region"])
    elif operation == "buckets-delete":
        results = buckets_delete(kwargs["bucket_name"])
    elif operation == "buckets-exists":
        results = buckets_exists(kwargs["bucket_name"])

    elif operation == "objects-list":
        results = objects_list(kwargs["bucket_name"], kwargs["object_name"])

    elif operation == "folders-create":
        results = folders_create(kwargs["bucket_name"], kwargs["folder_name"], kwargs["object_name"])
    elif operation == "folders-delete":
        results = folders_delete(kwargs["bucket_name"], kwargs["folder_name"])
    elif operation == "folders-exists":
        results = folders_exists(kwargs["bucket_name"], kwargs["folder_name"])

    elif operation == "files-upload":
        results = files_upload(kwargs["bucket_name"], kwargs["folder_name"], 
                                kwargs["file_name"], kwargs["object_name"])

    return results


def print_starter():
    print()
    print("************************ Main ************************")


def print_results(results):
    print()
    print()
    print(results)
    print()
    print("************************ End ************************")
    print()


def main():

    print_starter()

    arguments = parse_arguments()

    # read operation and its mandatory options from arguments dynamically
    # e.g. operation = arguments["operation"]

    # Bucket List Operation
    operation = "buckets-list"
    # results = execute_operation(operation)

    # Bucket Create Operation
    operation = "buckets-create"
    bucket = "23749028374982374981323"
    # region = None
    region = "us-west-1"  # Testing with diff region succeded

    # "ap-south-1" is the default region in AWS account when accessing through India
    # results = execute_operation(operation, bucket_name=bucket, region=region)


    # Bucket Delete Operation
    operation = "buckets-delete"
    bucket = "ryu32341312312"
    # results = execute_operation(operation, bucket_name=bucket)
    

    # Bucket Delete Operation
    operation = "buckets-exists"
    bucket = "ryucoder"
    # results = execute_operation(operation, bucket_name=bucket)



    # Objects List Operation
    operation = "objects-list"
    # bucket = "ryucoder"
    bucket = "aws-big-data-project"
    obj = "panama"
    # results = execute_operation(operation, bucket_name=bucket, object_name=obj)


    # Folder Create Operation
    operation = "folders-create"
    bucket = "ryucoder"
    folder = "3"
    # obj = "panama"
    
    # bucket = None 
    # folder = None 
    obj = None 

    # results = execute_operation(operation, bucket_name=bucket, folder_name=folder, object_name=obj)


    # Folder Delete Operation
    operation = "folders-delete"
    bucket = "ryucoder"
    folder = "panama"

    # bucket = None 
    # folder = None 
    
    # results = execute_operation(operation, bucket_name=bucket, folder_name=folder)


    # Folder Exists Operation
    operation = "folders-exists"
    bucket = "ryucoder"
    folder = "panama" # its not present but still showing as there is a panama3
    folder = "panama3"

    # bucket = None 
    # folder = None 
    
    # results = execute_operation(operation, bucket_name=bucket, folder_name=folder)


    # Files Upload Operation
    operation = "files-upload"
    bucket = "ryucoder"
    folder = "3"
    fi = "upload_me.txt"
    obj = "panama"
    
    # bucket = None 
    # folder = None 
    # fi = None 
    # obj = None 

    results = execute_operation(operation, bucket_name=bucket, folder_name=folder, 
                                file_name=fi, object_name=obj)


    print_results(results)


if __name__ == '__main__':
    main()